#!/bin/bash
set -e

influx bucket create \
    -o ${DOCKER_INFLUXDB_INIT_ORG} \
    -n glances \
    -r ${DOCKER_INFLUXDB_INIT_RETENTION}

GLANCES_BUCKET_ID=$(influx bucket list -n glances --hide-headers | awk '{ print $1 }')

influx v1 dbrp create \
    -o ${DOCKER_INFLUXDB_INIT_ORG} \
    --bucket-id ${GLANCES_BUCKET_ID} \
    --db glances \
    --rp glances \
    --default

influx v1 auth create \
    -o ${DOCKER_INFLUXDB_INIT_ORG} \
    --username ${INFLUXDB_USER} \
    --password ${INFLUXDB_PASSWORD} \
    --read-bucket ${GLANCES_BUCKET_ID} \
    --write-bucket ${GLANCES_BUCKET_ID}
