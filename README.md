# DD monitoring

This repository has two parts:

- `dd/` -> This docker-compose is run inside the same machine that Digital Democratic is running at
- `monitoring/` -> This docker-compose is run in the machine that does the monitoring

In order to configure it, you must copy the `.conf.example` file and name it removing the `.example` suffix. All the values **MUST** be set. 

## Quick Start

```
cd dd
cp dd.conf.example dd.conf
docker-compose up -d

cd ../monitoring
cp monitoring.conf.example monitoring.conf
docker-compose up -d
```