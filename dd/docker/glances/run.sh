#!/bin/sh

eval "echo \"$(cat /glances/conf/glances.conf.template)\" > /glances/conf/glances.conf"

python -m glances -C /glances/conf/glances.conf --export influxdb -q --disable-process
